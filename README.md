# GapstarsPhotoSortApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## How it works. 
 - Once the project is built or served , navigate to http://localhost:4200
 - Then select/ toggle on any image from the bottom container
 - Drag and sort the order of the selected images
 - Save changes using the button

## description

Once you select and sort images and make changes saved. you can refresh the page or revisit the site later.
This project saves your previous changes with the choice you made for the images