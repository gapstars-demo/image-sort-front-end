import { Component, Input, OnInit } from '@angular/core';
import { BaseService } from '../services/base-service.service';
import _ from 'lodash';
import { Output, EventEmitter } from '@angular/core';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-photo-selector',
  templateUrl: './photo-selector.component.html',
  styleUrls: ['./photo-selector.component.scss']
})
/**
* PhotoSelectorComponent is to provide the image selection capability.
* 
*/
export class PhotoSelectorComponent implements OnInit {
  public imageArray: any = [];
  public selectedImages: any = [];
  public maxSelection: number = 9;
  /** 
  *  outputImageArray is an object array that contains the selected images 
  *  imageOrder is the input of the component which gets the ordered list from previously saved state
  */
  @Output() outputImageArray = new EventEmitter<any[]>();
  @Input() imageOrder = [];
  constructor(
    /**
     *  BaseService is the service file that uses to communicate with API's
     *  In this case it uses only one service file.
     */
    private baseService: BaseService
  ) { }

  ngOnInit(): void {
    this.loadImages();
  }

  // load all the images from give test end-point
  loadImages() {
    const selectedImages = this.imageOrder.map((image: any) => image.id);
    const selectedImageMap = _.keyBy(this.imageOrder, image => image.id);
    this.baseService.getImages().subscribe((res: any) => {
      const { entries = [] } = res;
      this.imageArray = entries.map(imagedata => {
        if (selectedImages.some((imageId: any) => imageId === imagedata.id)) {
          imagedata.isActive = true;
          imagedata.index = selectedImageMap[imagedata.id].index;
          this.selectedImages.push(imagedata)
        } else {
          imagedata.isActive = false;
        }

        return imagedata;
      });
      this.updateSelectedView();
    }, err => {
      console.log(err);
    });

  }
  /**
   * updateSelected view is the function that uses to update the dome every time a user make a changes 
   * And in the first time it loads component.
   * @description array is sorted by its index number which is previously saved 
   */
  updateSelectedView() {
    this.selectedImages = this.selectedImages.filter((image: any) => image.isActive);
    this.outputImageArray.emit(_.orderBy(this.selectedImages, (image: any) => image.index))
  }

  changeSelectedPhoto(data: any, event: any) {
    const { id: inputImageId, isActive } = data;
    const { currentTarget: { checked } } = event;
    if (this.selectedImages.length < 10 && checked) {
      this.imageArray.map(image => {
        const { id } = image;
        if (id === inputImageId) {
          image.isActive = true;
          this.selectedImages.push(image)
        };
      })
    } else if (!checked) {
      this.imageArray.map(image => {
        const { id } = image;
        if (id === inputImageId) {
          image.isActive = false;
          this.selectedImages.push(image)
        };
      })
    } else {
      Swal.fire('Maximum image count exceeded', `you can only select up to ${this.maxSelection} images`, 'warning');
      event.currentTarget.checked = false;
    }
    this.updateSelectedView()
  }
}
