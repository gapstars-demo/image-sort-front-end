import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public selectedImages: any = [];
  public imageOrder: any = [];
  public imageOrderLoading = true;
  ngOnInit(): void {

  }

  modifyImageSelection(data: any) {
    this.selectedImages = data;
  }

  modifyImageOrder(data: any) {
    this.imageOrder = data;
    this.imageOrderLoading = false
  }
}
