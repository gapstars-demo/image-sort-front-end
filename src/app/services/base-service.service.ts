/**
 * @field all the service functions are written here
 * @author Sahan Ridma
 * @description this service file is only to service sorting function at the moment. when there are more functions, base service should be separated to service common requests pn;y
 */

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class BaseService {
  /**
   * serviceURL and dummyEndpoint are referenced form environment file. 
   */
  private serviceUrl = environment.API_URL;
  private dummyEndpoint = environment.DUMMY_ENDPOINT;
  constructor(private http: HttpClient) {

  }

  public getImages(): Observable<any> {
    return this.http.get(this.dummyEndpoint).pipe(map((res: any) => res))
  }

  public getOrder(url: string): Observable<any> {
    return this.http.get(`${this.serviceUrl}/${url}`).pipe(map((res: any) => res))
  }

  public setOrder(url: string,data: any): Observable<any> {
    return this.http.post<any>(`${this.serviceUrl}/${url}`, data).pipe(map((res: any) => res))
  }
}
