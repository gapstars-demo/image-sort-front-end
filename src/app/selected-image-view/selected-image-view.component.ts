import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BaseService } from '../services/base-service.service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import _ from 'lodash';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-selected-image-view',
  templateUrl: './selected-image-view.component.html',
  styleUrls: ['./selected-image-view.component.scss']
})
export class SelectedImageViewComponent implements OnInit {
  @Input() selectedImages = [];
  @Output() imageOrder: any = new EventEmitter<any>();
  constructor(
    private baseService: BaseService
  ) { }

  ngOnInit(): void {
    this.getImageOrder();
  }

  getImageOrder() {
    this.baseService.getOrder('sort').subscribe(res => {
      if (!_.isNull(res.data)) {
        this.imageOrder.emit(res.data.sortedList)
      } else {
        this.imageOrder.emit([])
      }
    }, err => console.log(err)
    )
  }
  drop(event: CdkDragDrop<any[]>) {
    console.log(event.item.data, event.currentIndex);
    moveItemInArray(this.selectedImages, event.previousIndex, event.currentIndex);
  }

  saveOrder() {
    const payload = this.selectedImages.map((image, index) => {
      return { id: image.id, index }
    });
    this.baseService.setOrder('sort',{ sortedList: payload }).subscribe(res => {
      Swal.fire('Successful', 'we updated your selection', 'success');
    }, err => console.log(err)
    )


  }
}
