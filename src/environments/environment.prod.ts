export const environment = {
  production: true,
  API_URL: 'http://localhost:4060/gapstar',
  DUMMY_ENDPOINT: 'https://dev-pb-apps.s3-eu-west-1.amazonaws.com/collection/CHhASmTpKjaHyAsSaauThRqMMjWanYkQ.json'
};
